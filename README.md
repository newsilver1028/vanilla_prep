# 바닐라 코딩 Prep [Mini Web Projects]

## 1. Background Changer
### 요구사항
#### ✏️ 랜덤한 Hex color code를 생성하는 함수 작성하기 (직접 작성해보세요.)

#### ✏️ 버튼 클릭 시, 랜덤한 Hex color code를 생성하여 페이지의 배경 색깔과 Hex color code 텍스트 수정하기
**[요구사항] Day 1**
- 위 이미지와 같은 페이지 만들기

**[요구사항] Day 2**
- 랜덤한 Hex color code를 생성하는 함수 작성하기 (직접 작성해보세요.)

> Hex Code란? </br>
0\~9까지의 숫자와 A\~F까지의 알파벳이 랜덤하게 구성되어 이루는 6자리 코드를 의미합니다. </br>예를 들면 000000, 3474FF 등 모두 유효한 Hex Code입니다. CSS에서는 Hex Code앞에 #를 붙여 색상값으로 이용할 수 있습니다. </br>
예) #3474FF

**[요구사항] Day 3**
- 버튼 클릭 시, 랜덤한 Hex color code를 생성하여 페이지의 배경 색깔과 Hex color code 텍스트 수정하기

### 출력화면

<img width="961" alt="HEX COLOR #70C6E1" src="https://user-images.githubusercontent.com/79626675/122031166-69ccf780-ce09-11eb-9c76-14f153c6d7af.png">

## 2. Carousel
### 요구사항
#### ✏️ 아래에 주어진 이미지와 같이 생긴 화면을 만들어 아래 요구사항의 기능을 구현해보세요.

<img alt="Screen_Shot_2020-05-06_at_9 19 04_AM" src="https://user-images.githubusercontent.com/79626675/122633657-aacc5100-d114-11eb-85ad-345e7814be8c.png">

> 🚨 애니메이션 효과는 필요하지 않습니다.
#### TODO
**[요구사항] Day 1** </br>
- 페이지 로딩 시, 첫 번째 이미지 화면에 보이기
- 좌측, 우측 화살표 두개 보이기
- 화면 아래 쪽에 Dot 다섯개 보이기

**[요구사항] Day 2**
- 좌측 화살표 클릭시 이전 이미지 보여주기
- 우측 화살표 클릭시 다음 이미지 보여주기

**[요구사항] Day 3**
- 5번째 이미지에서 우측 화살표를 누를 경우, 1번째 이미지 보여주기
- 1번째 이미지에서 좌측 화살표를 누를 경우, 5번째 이미지 보여주기
- 이미지 하단의 Dot를 누를 경우, 해당 순번의 이미지 보여주기

### 출력화면
![40E97971-5814-4EAF-8001-078534BA9B79](https://user-images.githubusercontent.com/79626675/122633591-5f19a780-d114-11eb-82c6-82bcff854f80.jpeg)
